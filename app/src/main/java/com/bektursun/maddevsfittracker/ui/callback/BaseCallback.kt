package com.bektursun.maddevsfittracker.ui.callback

interface BaseCallback {

    fun error(e: Throwable)
}