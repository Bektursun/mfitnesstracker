package com.bektursun.maddevsfittracker.ui.callback

import com.bektursun.maddevsfittracker.repository.db.model.Track

interface TracksCallback : BaseCallback {

    fun onTracksLoaded(list: List<Track>)
}