package com.bektursun.maddevsfittracker.ui.activity

import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bektursun.maddevsfittracker.R
import com.bektursun.maddevsfittracker.repository.DataManager
import com.bektursun.maddevsfittracker.repository.db.model.LocationUpdate
import com.bektursun.maddevsfittracker.service.LocationForegroundService
import com.bektursun.maddevsfittracker.ui.callback.MapCallback
import com.bektursun.maddevsfittracker.util.*
import com.bektursun.maddevsfittracker.util.ui.JoggingDetailFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.SphericalUtil
import kotlinx.android.synthetic.main.activity_map.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback, MapCallback {

    private var myReceiver: MyReceiver? = null

    private var service: LocationForegroundService? = null

    private var bound = false

    var map: GoogleMap? = null
    var location: Location? = null
    lateinit var dm: DataManager

    lateinit var dialogDetail: JoggingDetailFragment
    var lat = 0.0
    var lon = 0.0
    var trackId: Long = 0L

    var maxSpeed = 0f
    var minSpeed = 0f
    var avgSpeed = 0f
    var startTime = 0L
    var distance = 0.0
    var accuracy = 0f

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            service = null
            bound = false
        }

        override fun onServiceConnected(name: ComponentName?, binderService: IBinder?) {
            val binder = binderService as LocationForegroundService.LocalBinder
            service = binder.service
            bound = true
        }
    }

    val timer = object : CountUpTimer(1000) {
        override fun onTick(elapsedTime: Long) {
            val startTime = getId()
            txtTimer.text = formattedTime(startTime)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myReceiver = MyReceiver()
        setContentView(R.layout.activity_map)

        dialogDetail = JoggingDetailFragment()

        trackId = intent?.extras?.getLong("id", 0L) ?: 0L
        lat = intent.extras?.getDouble("lat", 0.0) ?: 0.0
        lon = intent.extras?.getDouble("lon", 0.0) ?: 0.0
        startTime = getId()

        dm = applicationContext.dm()

        dm.loadLocations(this, trackId)

        calculateStatisticsAndUpdate(emptyList())

        val fragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        fragment.getMapAsync(this)

        fabStop.setOnClickListener {
            dm.clear()
            service?.removeLocationUpdates()
            val endTime = System.currentTimeMillis()
            val elapsedTime = endTime - startTime
            dm.updateTrack(trackId = trackId,
                startTime = startTime,
                endTime = endTime,
                elapsedTime = elapsedTime,
                minSpeed = minSpeed,
                maxSpeed = maxSpeed,
                avgSpeed = avgSpeed,
                accuracy = accuracy,
                distance = distance)
            putId(-1L)
            startTrackActivity()
        }
    }

    override fun onLocationsLoaded(list: List<LocationUpdate>) {
        if (list.isEmpty()) {
            return
        }
        map?.clear()
        calculateStatisticsAndUpdate(list)
        val options = PolylineOptions()
        options.width(15f)
        options.color(Color.BLACK)
        var latLng = LatLng(0.0, 0.0)
        for (update in list) {
            latLng = LatLng(update.lat, update.lon)
            options.add(LatLng(update.lat, update.lon))
        }
        map?.addMarker(MarkerOptions()
            .position(latLng)
            .title("Me")
            .icon(newIcon(R.drawable.ic_jogging)))
        map?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        map?.addPolyline(options)
    }

    @SuppressLint("SetTextI18n")
    private fun calculateStatisticsAndUpdate(list: List<LocationUpdate>) {
        val max = list.maxBy { it.speed }
        if (max != null) {
            maxSpeed = max.speed
        }
        val min = list.minBy { it.speed }
        if (min != null)
            minSpeed = min.speed
        val avg = list.sumBy { it.speed.toInt() }
        val size = if (list.isEmpty()) 1 else list.size
        avgSpeed = avg * 1f / size
        distance = SphericalUtil.computeLength(list.map { it.getLatLng() }.toMutableList())
        if (list.lastIndex != -1)
            accuracy = list[list.lastIndex].accuracy

        txtStartTime.text = getTime(startTime)

        txtAvgSpeed.text = "${avgSpeed.limitDecimal()}m/s"
        txtMaxSpeed.text = "${maxSpeed.limitDecimal()}m/s"
        txtMinSpeed.text = "${minSpeed.limitDecimal()}m/s"

        txtDistance.text = "${distance.toInt()}m"
        txtAccuracy.text = "${accuracy.toInt()}%"
    }

    override fun error(e: Throwable) {
        Toast.makeText(this@MapActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onMapReady(p0: GoogleMap?) {
        map = p0
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(
            LatLng(lat, lon),
            18f
        ))
        if (dm.locationUpdates.isNotEmpty()) {
            onLocationsLoaded(dm.locationUpdates)
        } else {
            map?.addMarker(MarkerOptions()
                .position(LatLng(lat, lon))
                .title("Me")
                .icon(newIcon(R.drawable.ic_jogging)))
        }
    }

    override fun onStart() {
        super.onStart()

        Handler().postDelayed({
            service?.requestLocationUpdates()
            service?.trackId = trackId
        }, 1000)

        timer.start()

        bindService(Intent(this, LocationForegroundService::class.java), serviceConnection,
            Context.BIND_AUTO_CREATE)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver!!,
            IntentFilter(LocationForegroundService.ACTION_BROADCAST)
        )
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver!!)
        timer.stop()
        super.onPause()
    }

    override fun onStop() {
        if (bound) {
            unbindService(serviceConnection)
            bound = false
        }
        super.onStop()
    }

    private inner class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val newLocation = intent.getParcelableExtra<Location>(LocationForegroundService.EXTRA_LOCATION)
            if (newLocation != null) {
                location = newLocation

                dm.loadLocations(this@MapActivity, trackId)

                Toast.makeText(this@MapActivity, LocationServiceUtil.getLocationText(location), Toast.LENGTH_SHORT).show()
            }
        }
    }
}