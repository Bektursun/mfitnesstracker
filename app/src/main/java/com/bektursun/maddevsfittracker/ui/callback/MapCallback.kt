package com.bektursun.maddevsfittracker.ui.callback

import com.bektursun.maddevsfittracker.repository.db.model.LocationUpdate

interface MapCallback : BaseCallback {

    fun onLocationsLoaded(list: List<LocationUpdate>)
}