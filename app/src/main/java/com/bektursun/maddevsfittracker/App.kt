package com.bektursun.maddevsfittracker

import android.app.Application
import com.bektursun.maddevsfittracker.repository.DataManager
import com.bektursun.maddevsfittracker.repository.db.AppDatabase

class App : Application() {

    lateinit var db: AppDatabase
    lateinit var dm: DataManager

    override fun onCreate() {
        super.onCreate()
        db = AppDatabase.dbInstance(this)
        dm = DataManager(db)
    }
}