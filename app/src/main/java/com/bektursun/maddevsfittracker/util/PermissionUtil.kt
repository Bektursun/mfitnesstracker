package com.bektursun.maddevsfittracker.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

object PermissionUtil {

    fun checkPermissions(context: Context): Boolean {
        for (permission in ALL_APP_PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun isAccessFineGranted(activity: AppCompatActivity): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(activity,
            Manifest.permission.ACCESS_FINE_LOCATION)
    }

    fun requestPermissions(activity: AppCompatActivity) {
        ActivityCompat.requestPermissions(activity,
            ALL_APP_PERMISSIONS,
            ALL_PERMISSION_CODE)
    }
}