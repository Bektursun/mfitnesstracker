package com.bektursun.maddevsfittracker.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bektursun.maddevsfittracker.App
import com.bektursun.maddevsfittracker.repository.DataManager
import com.bektursun.maddevsfittracker.repository.db.AppDatabase
import com.bektursun.maddevsfittracker.repository.db.model.LocationUpdate
import com.bektursun.maddevsfittracker.ui.activity.MapActivity
import com.bektursun.maddevsfittracker.ui.activity.TracksActivity
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

fun Context.db(): AppDatabase {
    return (this as App).db
}

fun Context.dm(): DataManager {
    return (this as App).dm
}

fun Location.toLocationUpdate(id: Long, trackId: Long): LocationUpdate {
    return LocationUpdate(
        id,
        lat = latitude,
        lon = longitude,
        speed = speed,
        trackId = trackId,
        accuracy = accuracy
    )
}

fun AppCompatActivity.startMapActivity(id: Long, lat: Double, lon: Double) {
    val intent = Intent(this, MapActivity::class.java)
    intent.putExtra("id", id)
    intent.putExtra("lat", lat)
    intent.putExtra("lon", lon)
    startActivity(intent)
    finish()
}

fun AppCompatActivity.newIcon(id: Int): BitmapDescriptor {
    return BitmapDescriptorFactory.fromResource(id)
}

fun AppCompatActivity.showMessage(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.putId(value: Long) {
    val preferences = getSharedPreferences("App_Preferences", Context.MODE_PRIVATE)
    val editor = preferences.edit()
    editor.putLong("id", value)
    editor.commit()
}

fun Context.getId(): Long {
    return getSharedPreferences("App_Preferences", Context.MODE_PRIVATE).getLong("id", -1L)
}

fun Context.putLatLng(lat: Double, lon: Double) {
    val preferences = getSharedPreferences("App_Preferences", Context.MODE_PRIVATE)
    val editor = preferences.edit()
    editor.putFloat("lat", lat.toFloat())
    editor.putFloat("lon", lon.toFloat())
    editor.commit()
}

fun Context.getLatLng(): LatLng {
    val pref = getSharedPreferences("App_Preferences", Context.MODE_PRIVATE)
    val lat = pref.getFloat("lat", 0f)
    val lon = pref.getFloat("lon", 0f)
    return LatLng(lat.toDouble(), lon.toDouble())
}

fun AppCompatActivity.startTrackActivity() {
    val intent = Intent(this, TracksActivity::class.java)
    startActivity(intent)
    finish()
}

@SuppressLint("SimpleDateFormat")
fun getTime(millis: Long): String {
    val formatter = SimpleDateFormat("HH:mm")
    val date = Date()
    date.time = millis
    return formatter.format(date)
}

fun calculateInMinutes(millis: Long): String {
    val result = millis / 1000 / 60
    return "$result minutes"
}

fun formattedTime(startTime: Long): String {
    val current = System.currentTimeMillis()
    var final = current - startTime
    final += 1000
    final /= 1000
    var hourText = ""
    var minutesText = ""
    var secondsText = ""
    if (final < 60) {
        hourText = "00"
        minutesText = "00"
        secondsText = if (final < 10) "0$final" else "$final"
    } else if (final in 60..3599) {
        val min = final / 60
        val sec = final % 60
        hourText = "00"
        minutesText = if (min < 10) "0$min" else "$min"
        secondsText = if (sec < 10) "0$sec" else "$sec"
    }
    return "$hourText:$minutesText:$secondsText"
}

fun Float.limitDecimal(): String {
    return DecimalFormat("##.##").format(this)
}
