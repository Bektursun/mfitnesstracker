package com.bektursun.maddevsfittracker.util

import android.content.Context
import android.location.Location
import android  .preference.PreferenceManager
import com.bektursun.maddevsfittracker.R
import java.text.DateFormat
import java.util.*

object LocationServiceUtil {

    val KEY_REQUESTION_LOCATION_UPDATES = "requesting_location_updates"

    @Suppress("DEPRECATION")
    fun requestingLocationUpdates(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getBoolean(KEY_REQUESTION_LOCATION_UPDATES, false)
    }

    @Suppress("DEPRECATION")
    fun setRequestingLocationUpdates(context: Context, requestingLocationUpdates: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(KEY_REQUESTION_LOCATION_UPDATES, requestingLocationUpdates)
            .apply()
    }

    fun getLocationText(location: Location?): String {
        return if (location == null)
            "Unknown location"
        else
            "(" + location.latitude + ", " + location.longitude + ",speed:${location.speed}" + ")"
    }

    fun getLocationTitle(context: Context): String {
        return context.getString(R.string.location_updated, DateFormat.getDateTimeInstance().format(
            Date()
        ))
    }
}