package com.bektursun.maddevsfittracker.util.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bektursun.maddevsfittracker.R
import com.bektursun.maddevsfittracker.repository.db.model.Track
import com.bektursun.maddevsfittracker.util.getTime
import kotlinx.android.synthetic.main.dialog_jogging_dialog.*

class JoggingDetailFragment : DialogFragment() {

    lateinit var track: Track

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_jogging_dialog, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtStartTime.text = "Start at ${getTime(track.startTime)}"

        txtAvgSpeed.text = "Avg.speed:${track.avgSpeed}m/s"
        txtMaxSpeed.text = "Max speed:${track.maxSpeed}m/s"
        txtMinSpeed.text = "Min speed:${track.minSpeed}m/s"

        txtDistance.text = "Distance:${track.distance.toInt()}m"
        txtAccuracy.text = "Accuracy:${track.accuracy.toInt()}%"
    }
}