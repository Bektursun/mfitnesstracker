package com.bektursun.maddevsfittracker.util

const val DATABASE_NAME = "appdatabase"

val ALL_APP_PERMISSIONS = arrayOf(
    android.Manifest.permission.ACCESS_FINE_LOCATION,
    android.Manifest.permission.ACCESS_COARSE_LOCATION
)

const val ALL_PERMISSION_CODE = 11

const val APP_LOG = "AppLog"