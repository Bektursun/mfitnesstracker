package com.bektursun.maddevsfittracker.repository

import android.annotation.SuppressLint
import com.bektursun.maddevsfittracker.repository.db.AppDatabase
import com.bektursun.maddevsfittracker.repository.db.model.LocationUpdate
import com.bektursun.maddevsfittracker.repository.db.model.Track
import com.bektursun.maddevsfittracker.ui.callback.MapCallback
import com.bektursun.maddevsfittracker.ui.callback.TracksCallback
import com.bektursun.maddevsfittracker.util.IO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DataManager(val db: AppDatabase) {

    val locationUpdates = mutableListOf<LocationUpdate>()

    @SuppressLint("CheckResult")
    fun loadTracks(callback: TracksCallback) {
        db.trackDao().getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onTracksLoaded(it)
            }, {
                callback.error(it)
            })
    }

    @SuppressLint("CheckResult")
    fun loadLocations(callback: MapCallback, trackId: Long) {
        db.locationUpdateDao().getAll(trackId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                locationUpdates.clear()
                locationUpdates.addAll(it)
                callback.onLocationsLoaded(it)
            }, {
                callback.error(it)
            })
    }

    fun updateTrack(
        trackId: Long,
        startTime: Long,
        endTime: Long,
        elapsedTime: Long,
        minSpeed: Float,
        maxSpeed: Float,
        avgSpeed: Float,
        accuracy: Float,
        distance: Double
    ) {
        val track = Track(
            id = trackId,
            startTime = startTime,
            endTime = endTime,
            elapsedTime = elapsedTime,
            minSpeed = minSpeed,
            maxSpeed = maxSpeed,
            avgSpeed = avgSpeed,
            accuracy = accuracy,
            distance = distance
        )
        IO.execute {
            db.trackDao().insert(track)
        }
    }

    fun clear() {
        locationUpdates.clear()
    }
}