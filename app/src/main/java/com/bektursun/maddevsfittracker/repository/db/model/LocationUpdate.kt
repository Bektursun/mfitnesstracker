package com.bektursun.maddevsfittracker.repository.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng

@Entity(tableName = "location")
data class LocationUpdate(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Long,
    @ColumnInfo(name = "lat")
    val lat: Double,
    @ColumnInfo(name = "lon")
    val lon: Double,
    @ColumnInfo(name = "speed")
    val speed: Float,
    @ColumnInfo(name = "accuracy")
    val accuracy: Float,
    @ColumnInfo(name = "track_id")
    val trackId: Long
) {
    @Ignore
    override fun toString(): String {
        return "id:$id,lat:$lat,lon:$lon,speed:$speed"
    }

    @Ignore
    fun getLatLng(): LatLng {
        return LatLng(lat, lon)
    }
}